<?php

namespace Drupal\simple_content_notifications\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure settings for the content needing review notifications.
 */
class ContentReviewNotificationSettingsForm extends ConfigFormBase {
  const SETTINGS = 'simple_content_notifications.review_settings';

  /**
   * The state store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Class constructor.
   */
  final public function __construct(
    ConfigFactoryInterface $config_factory,
    StateInterface $state) {
    parent::__construct($config_factory);
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the services required to construct this class.
      $container->get('config.factory'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'simple_content_review_notification_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $form['review_notifications_active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Content Review Notifications'),
      '#default_value' => $config->get('review_notifications_active'),
      '#return_value' => TRUE,
    ];
    $form['review_notifications_addresses'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email Addresses'),
      '#description' => $this->t('Comma-separated list of email addresses where content review notifications will be sent.'),
      '#default_value' => $config->get('review_notifications_addresses'),
      '#required' => TRUE,
    ];
    $form['review_notifications_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Last Review Date Field'),
      '#description' => $this->t('Drupal machine name of the field containing the Last Revision Date information.'),
      '#default_value' => $config->get('review_notifications_field'),
      '#required' => TRUE,
    ];
    $form['review_notifications_due'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Time Before Review'),
      '#description' => $this->t('How long must pass before review is due; expressed as negative relative date; recommended value is "-6 months".'),
      '#default_value' => $config->get('review_notifications_due'),
      '#required' => TRUE,
    ];
    $next_send_state = $this->state->get('simple_content_notifications.review_notifications_next_send');
    if (!$next_send_state) {
      $next_send_datetime = new \DateTime('+1 day');
      $next_send_state = $next_send_datetime->format('Y-m-d');
    }
    $form['review_notifications_next_send'] = [
      '#type' => 'date',
      '#title' => $this->t('Next notification send date'),
      '#description' => $this->t("The next notification date is stored in the site's state. This will therefore not be deployed in a deployment workflow and must be set per environment. If not yet set, it will be automatically set to 1 day from the current time on the next cron run. A date in the past likely indicates that cron is not running frequently enough."),
      '#default_value' => $next_send_state,
      '#required' => TRUE,
    ];
    $form['review_notifications_delay'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Time Between Notifications'),
      '#description' => $this->t('How long must pass before next notification is sent; expressed as positive relative date.'),
      '#default_value' => $config->get('review_notifications_delay'),
      '#required' => TRUE,
    ];
    $form['review_notifications_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Notification Email Subject'),
      '#default_value' => $config->get('review_notifications_subject'),
      '#required' => TRUE,
    ];
    $form['review_notifications_enable'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Content Review Notification Options'),
      '#tree' => TRUE,
    ];
    $form['review_notifications_enable']['published'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Only include published nodes'),
      '#default_value' => $config->get('review_notifications_published'),
      '#return_value' => TRUE,
    ];
    $form['review_notifications_enable']['in_dev'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow notifications in dev/test'),
      '#default_value' => $config->get('review_notifications_in_dev'),
      '#return_value' => TRUE,
    ];
    $form['review_notifications_enable']['live_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Production Domain'),
      '#description' => $this->t('The domain of the production version of the website.'),
      '#default_value' => $config->get('review_notifications_live_domain'),
      '#required' => TRUE,
    ];
    $form['review_notifications_enable']['logging'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable logging'),
      '#description' => $this->t('This logs an warnings into the watchdog log.'),
      '#default_value' => $config->get('review_notifications_logging'),
      '#return_value' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration to save other submitted values.
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration settings.
      ->set('review_notifications_addresses', $form_state->getValue('review_notifications_addresses'))
      ->set('review_notifications_field', $form_state->getValue('review_notifications_field'))
      ->set('review_notifications_due', $form_state->getValue('review_notifications_due'))
      ->set('review_notifications_delay', $form_state->getValue('review_notifications_delay'))
      ->set('review_notifications_subject', $form_state->getValue('review_notifications_subject'))
      ->set('review_notifications_active', $form_state->getValue('review_notifications_active'))
      ->set('review_notifications_published', $form_state->getValue([
        'review_notifications_enable',
        'published',
      ]))
      ->set('review_notifications_live_domain', $form_state->getValue([
        'review_notifications_enable',
        'live_domain',
      ]))
      ->set('review_notifications_in_dev', $form_state->getValue([
        'review_notifications_enable',
        'in_dev',
      ]))
      ->set('review_notifications_logging', $form_state->getValue([
        'review_notifications_enable',
        'logging',
      ]))
      ->save();

    // Store the next send date in State.
    $this->state->set('simple_content_notifications.review_notifications_next_send', $form_state->getValue('review_notifications_next_send'));

    parent::submitForm($form, $form_state);
  }

}
