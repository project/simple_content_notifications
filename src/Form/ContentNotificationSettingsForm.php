<?php

namespace Drupal\simple_content_notifications\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure settings for the content notifications.
 */
class ContentNotificationSettingsForm extends ConfigFormBase {
  const SETTINGS = 'simple_content_notifications.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'simple_content_notification_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['simple_content_notifications_active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Content Update Notifications'),
      '#default_value' => $config->get('simple_content_notifications_active'),
      '#return_value' => TRUE,
    ];
    $form['simple_content_notifications_addresses'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email Addresses'),
      '#description' => $this->t('Comma-separated list of email addresses where notifications will be sent.'),
      '#default_value' => $config->get('simple_content_notifications_addresses'),
      '#required' => TRUE,
    ];
    $form['simple_content_notifications_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Selected Content Types'),
      '#description' => $this->t('Generate notifications for the selected content types.'),
      '#options' => node_type_get_names(),
      '#default_value' => $config->get('simple_content_notifications_types') ?? [],
    ];
    $form['simple_content_notifications_enable'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Content Update Notification Options'),
      '#tree' => TRUE,
    ];
    $form['simple_content_notifications_enable']['in_dev'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow notifications in dev/test'),
      '#default_value' => $config->get('simple_content_notifications_in_dev'),
      '#return_value' => TRUE,
    ];
    $form['simple_content_notifications_enable']['live_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Production Domain'),
      '#description' => $this->t('The domain of the production version of the website.'),
      '#default_value' => $config->get('simple_content_notifications_live_domain'),
      '#required' => TRUE,
    ];
    $form['simple_content_notifications_enable']['logging'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable logging'),
      '#description' => $this->t('This logs an warnings into the watchdog log.'),
      '#default_value' => $config->get('simple_content_notifications_logging'),
      '#return_value' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration to save other submitted values.
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration settings.
      ->set('simple_content_notifications_addresses', $form_state->getValue('simple_content_notifications_addresses'))
      ->set('simple_content_notifications_types', $form_state->getValue('simple_content_notifications_types'))
      ->set('simple_content_notifications_active', $form_state->getValue('simple_content_notifications_active'))
      ->set('simple_content_notifications_live_domain', $form_state->getValue([
        'simple_content_notifications_enable',
        'live_domain',
      ]))
      ->set('simple_content_notifications_in_dev', $form_state->getValue([
        'simple_content_notifications_enable',
        'in_dev',
      ]))
      ->set('simple_content_notifications_logging', $form_state->getValue([
        'simple_content_notifications_enable',
        'logging',
      ]))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
