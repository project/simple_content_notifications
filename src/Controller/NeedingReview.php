<?php

namespace Drupal\simple_content_notifications\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides report of content that needs review.
 */
class NeedingReview extends ControllerBase {

  /**
   * Returns a simple page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function reportPage() {
    // Request report data. This will be FALSE if an error occurred.
    $report_data = simple_content_notifications_check_last_revised();
    $rows = ($report_data) ? count($report_data) : 0;

    return [
      '#cache' => ['max-age' => 0],
      '#report_data' => $report_data,
      '#rows' => $rows,
      '#theme' => 'needs_review_page',
    ];
  }

}
