# Simple Content Notifications

## Motivation

This module provides two functions:

- generate email notifications when content was added, updated, or deleted; and
- generate email notices about content that had gone 6 months without being
reviewed

There are other, more complicated modules that could be configured to provide
this functionality. However, I needed something simple.

## What this module does

### Content CRUD notifications

Notifications can be sent to one or more email addresses when content is added,
edited, or deleted. The email recipients can be configured in the module
settings (/admin/config/content/simple_content_notifications/settings).In the
settings, you can also select which node types should generate a notification.

### Content review notifications

A piece of content is determined to need review if a set amount of time
has passed since the content was last reviewed. The last reviewed date
needs to be stored in a custom field; the field machine name must be 
provided in the module settings 
(/admin/config/content/simple_content_notifications/needing_review_settings).Any 
content type that has the specified field will generate a review notification.

Because there could be many pieces of content that need review,
review notifications are not sent individually for each piece of content.
Instead, a notification is generated that lists all the content items
needing review. The notification is sent periodically. The frequency
of that notification is determined in the module settings.

The module also provides an admin page (/admin/content/needing_review) 
listing all content nodes needing review. Users with the "administer content"
permission can view this page.

If no next send date is set in an environment, the next send date will be
automatically set to tomorrow. At that point notifications will be sent for
any content marked as needing review on that date or earlier.

## Installation and setup

Install this module just as any other Drupal module. Then:

1. Configure the permission "Administer Content Notifications"; users with 
this permission can access the settings for both types of notifications.
2. Configure the content notifications (/admin/config/content/simple_content_notifications/settings)
3. Configure the content review notifications
 (/admin/config/content/simple_content_notifications/needing_review_settings)
